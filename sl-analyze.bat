# run analysis (Qwiet SAST) - post build 
SHIFTLEFT_SBOM_GENERATOR=2 
sl.exe analyze \
  --app "$CI_PROJECT_NAME" \
  --oss-project-dir "$CI_PROJECT_DIR" \
  --version-id "$CI_COMMIT_SHA" \
  --tag branch="$CI_COMMIT_REF_NAME" \
  --wait \
  --verbose \
  --csharp \
  netcoreWebapi/netcoreWebapi.csproj

# Check if this is running in a merge request
if %CI_MERGE_REQUEST_IID% != '' (
  # Run check-analysis and save report to /tmp/check-analysis.md
  sl check-analysis \
    --app "$CI_PROJECT_NAME" \
    --report \
    --report-file /tmp/check-analysis.md \
    --source "tag.branch=master" \
    --target "tag.branch=$CI_COMMIT_REF_NAME"

  CHECK_ANALYSIS_OUTPUT=$(print /tmp/check-analysis.md)
  COMMENT_BODY=$(jq -n --arg body "$CHECK_ANALYSIS_OUTPUT" '{body: $body}')
  export GITLAB_TOKEN=${GITLAB_API_TOKEN}
  # Post report as merge request comment
  curl -i -XPOST "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/merge_requests/$CI_MERGE_REQUEST_IID/notes" \
    -H "PRIVATE-TOKEN: $GITLAB_TOKEN" \
    -H "Content-Type: application/json" \
    -d "$COMMENT_BODY"
)
